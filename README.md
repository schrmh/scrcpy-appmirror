# scrcpy-appmirror
A project which aims to make it as easy as possible to stream individual apps from your Android 9+ phone by using scrcpy.  
The phone keeps being usable, so you can use other apps while streaming individual apps.

Currently using a work-around that creates a small simulated visible display on the phone.  
Due to the fact that only one of those can exist, it is only possible to stream one app at a time.

Some devices might have some problems because of vendor specific implementations.  
If you or the script messed up by using incompatible screen or density values, try to reset the values by using:
```
wm size reset
wm density reset
```
via `adb shell` and then reboot your phone if it doesn't take effect (try `adb kill-server` if phone isn't detected)



Findings:  
If I create overlay displays over 750 times (up to 1000), my phone reboots even tho it doesn't look like it's out of RAM.
