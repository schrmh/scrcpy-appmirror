#!/bin/sh
# Test if commands exist and if Android is ready to generate a simulated display
test $(command -v adb) || { printf "Please install the adb (Android Debug Bridge)\n"; exit; }
test $(command -v scrcpy) || { printf "Please install scrcpy\n"; exit; }
adb shell wm size 2>/dev/null 1>/dev/null || { printf "Either Android is not booted or wm size command is missing\n"; exit; }

# Require app name and activity name separated by a space, else print simplified help
test -z "$1" || test -z "$2" && {
 printf "Usage example (app, activity, width, height, dpi, title):\n"
 printf "./scrcpy-appmirror.sh com.android.launcher3 .activity.MainActivity 240 320 120 WindowTitle\n\n"
 printf " dpi (min 120; theory 72 to INT_MAX): https://developer.android.com/training/multiscreen/screendensities\n"
 printf "size (min 100): https://developer.android.com/guide/topics/large-screens/support-different-screen-sizes\n"
 exit
}

# Check for too small values (if width or height value/2 would be too small then set temporary value*2 the var value)
[ -n "$3" ] && { [ "$3" -ge 100 ] 2>/dev/null && { [ "$3" -lt 200 ] && tmp_w=$(($3*2)) || tmp_w=$3; } || { printf "width 
too small or invalid\n"; exit; }; }
[ -n "$4" ] && { [ "$4" -ge 100 ] 2>/dev/null && { [ "$4" -lt 200 ] && tmp_h=$(($4*2)) || tmp_h=$4; } || { printf "height 
too small or invalid\n"; exit; }; }
[ -n "$5" ] && { [ "$5" -ge 120 ] 2>/dev/null ||  { printf "dpi too small or invalid\n"; exit; }; }

# Create overlay with half of wanted size (100x100/120 if too small. By the way: wm resize can go up to 200%)
main_size=$(adb shell wm size | cut -d: -f2)
main_width=$(echo $main_size | cut -d"x" -f1 | tr -d " ")
main_height=$(echo $main_size | cut -d"x" -f2)
main_dpi=$(adb shell wm density | cut -d: -f2 | tr -d " ")
adb shell settings put global overlay_display_devices $((${tmp_w:-$main_width}/2))x$((${tmp_h:-$main_height}/2))/${5:-$main_dpi}
sleep 0.1

# Get last display ID (simulated one, hopefully. TODO: Check DisplayId changes)
# (TODO: maybe use dumpsys SurfaceFlinger --display-id to identify number of hardware displays... but what about folding?)
simulated=$(adb shell dumpsys display displays | awk -F= '/mDisplayId/{print $2}' | tail -n1)
[ $simulated -eq 0 ] && { # TODO: Maybe later not necessary anymore when DisplayId changes tracking is implemented
 printf "Case 1: Got value 0 which is usually the main display. Will exit to prevent changes to main display.\n"
 printf "adb shell dumpsys display displays | grep mDisplayId\n"
 printf "will tell you how many display exist. If only one, then the simulated screen creation failed.\n"
 exit
}

# Reset any overwrite values (may be carried from previous overlay instance)
adb shell wm size reset -d $simulated
adb shell wm density reset -d $simulated
sleep 0.1

# Resize to intended resolution
adb shell wm size ${3:-$main_width}x${4:-$main_height} -d $simulated
adb shell wm density ${5:-$main_dpi} -d $simulated
sleep 0.1

# Create the smallest possible overlay (overwrite values will keep existing)
# (Overlay won't exist when values are below 100x100/120)
adb shell settings put global overlay_display_devices 100x100/${5:-$main_dpi}
sleep 0.1

# Get new last display ID (simulated one, hopefully. TODO: Check DisplayId changes)
simulated=$(adb shell dumpsys display displays | awk -F= '/mDisplayId/{print $2}' | tail -n1)
[ $simulated -eq 0 ] && { # TODO: Maybe later not necessary anymore when DisplayId changes tracking is implemented
 printf "Case 2: Got value 0 which is usually the main display. Will exit to prevent changes to main display.\n"
 printf "adb shell dumpsys display displays | grep mDisplayId\n"
 printf "will tell you how many display exist. If only one, then the simulated screen creation failed.\n"
 exit
}

# Launch activity in simulated display. Use windowingMode 1 (maybe not necessary?)
# https://android.googlesource.com/platform/frameworks/base/+/master/core/java/android/app/WindowConfiguration.java
# 0 WINDOWING_MODE_UNDEFINED
# 1 WINDOWING_MODE_FULLSCREEN
# 2 WINDOWING_MODE_PINNED
# 3 WINDOWING_MODE_SPLIT_SCREEN_PRIMARY
# 4 WINDOWING_MODE_SPLIT_SCREEN_SECONDARY
# 5 WINDOWING_MODE_FREEFORM
# 6 WINDOWING_MODE_MULTI_WINDOW (Added in Android 11)
# (TODO: Detect vendor and Android version specific windowingMode stuff. Defined in WindowConfiguration.java)
# (TODO: App selection via imagemagick, sxiv or something? Alternatively cli to quickly switch apps at will)
adb shell am start-activity -S --windowingMode 1 --display $simulated "$1"/"$2"
scrcpy --display $simulated --window-title "${6-Mirror $simulated}"

# scrcpy is not running anymore
# Reset any overwrite values before doing that to prevent carrying over
adb shell wm size reset -d $simulated
adb shell wm density reset -d $simulated
sleep 0.1

# Close overlay
adb shell settings put global overlay_display_devices none
